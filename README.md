# OpenRSP 

Hardware and software to control the RSP-750-15 Power Supply primarly as a batter charger for a LiFePo battery bank incorporating storage and stand-by modes. Intial design will focus on a 4S setup of LIFEPO cells such as 3.2V SE200AHA CALB EV Lithium LiFePO4 Cell.

I've been unhappy with how dumb most "smart" battery chargers are and think given the low cost of computing these day there is room for much improvement. The prototype will use a single board computer to control the RSP-750 via a DAC/ACD. The main goal of the project is to provide a more intelligent way to handle short, medium and long duration charging windows. Battery chargers should recognize when you put your boat in a marina and are gone for a month and adapt the charging scheme. They should learn how you use your vessels power systems and adapt the charging regimen.

I/O List

Inputs
1. Cell 1 Voltage - Analog 
2. Cell 2 Voltage - Analog 
3. Cell 3 Voltage - Analog 
4. Cell 4 Voltage - Analog 
5. Overall Pack Voltage - Analog
6. Cell 1 Temprature - Analog 
7. Cell 2 Temprature - Analog 
8. Cell 3 Temprature - Analog 
9. Cell 4 Temprature - Analog
10. Mode Select (storage / stand-by / fast-charge) - Digital
11. BMS Charging Trouble
12. BMS Discharge Trouble
13. DC_OK, Low when PSU is powered on.

Outputs
1. Voltage Trim (Analog)
2. Current Trim (Analog)
3. Enable Charge (Digital)
4. Voltage Alarm (Digital)
5. Temprature Alarm (Digital)
6. Alert (Digital)
7. Alarm (Digital)

Hardware
1. RSP-750-15
2. Raspberry Pi 
3. Waveshare High Precision ADC/DAC Board - https://www.waveshare.com/wiki/High-Precision_AD/DA_Board
4. ESP8266 - wireless data collection 
5. Custom Cabling 

Software
1. Python service
2. InfluxDB to store data
3. Web interface 
4. Messaging service - telegram, e-mail, local

